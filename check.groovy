#!/usr/bin/env groovy

pipeline {
    agent any
    options {
        buildDiscarder(logRotator(daysToKeepStr: '15', numToKeepStr: '20'))
    }
    stages {
        stage("Initial-Validation") {
            steps{
                script {
                    if(params.BRANCH_NAME ==~ /origin\/test\/[A-Za-z]+-[0-9]+$/ || params.BRANCH_NAME ==~ /origin\/hotfix\/[A-Za-z]+-[0-9]+$/) {
                        echo "Correct branch $params.BRANCH_NAME"
                    }
                    else {
                        echo "Wrong branch $params.BRANCH_NAME"
                        error "Branch Name NOT VALID for ===> $params.branch"
                    }
                }
            }
        }
    }
}